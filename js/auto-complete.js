(function ($) {

$(function() {
        var webservice_url = "http://localhost/drupal7/sites/all/modules/ecardplus/service_demo/demodata.php";
        $("#Sender_name").focus();

		$( "#Sender_name" ).autocomplete({
			source: function( request, response ) {
				$.ajax({
					//url: webservice_url,
					url: webservice_url,
					dataType: "json",
					data: {
						name: request.term
					},
					success: function( data ) {
						response( $.map( data, function( item ) {
							return {
								label: item.LAST_NME + ', ' + item.FIRST_NME,
								value: item.FIRST_NME + ' ' + item.LAST_NME,
                                email: item.EMAIL
                                
							}
						}));
					}
				});
			},
			minLength: 3,
			select: function( event, ui ) {
                $('#edit-mail-from').val(ui.item.email);
                $('#Recipients_name').focus();
			}
		});

		$( "#Recipients_name" ).autocomplete({
			source: function( request, response ) {
				$.ajax({
					url: webservice_url,
					dataType: "json",
					data: {
						name: request.term
					},
					success: function( data ) {
						response( $.map( data, function( item ) {
							return {
								label: item.LAST_NME + ', ' + item.FIRST_NME,
								value: item.FIRST_NME + ' ' + item.LAST_NME,
                                email: item.EMAIL

							}
						}));
					}
				});
			},
			minLength: 3,
			select: function( event, ui ) {
                $('#edit-mail-to').val(ui.item.email);
                $('#edit-text').focus();
			}
		});

		$( "#Manager_name" ).autocomplete({
			source: function( request, response ) {
				$.ajax({
					url: webservice_url,
					dataType: "json",
					data: {
						name: request.term
					},
					success: function( data ) {
						response( $.map( data, function( item ) {
							return {
								label: item.LAST_NME + ', ' + item.FIRST_NME,
								value: item.FIRST_NME + ' ' + item.LAST_NME,
                                email: item.EMAIL

							}
						}));
					}
				});
			},
			minLength: 3,
			select: function( event, ui ) {
                $('#edit-mail-manager').val(ui.item.email);
                $('#edit-text').focus();
			}
		});
	});
    
})(jQuery);
