<?php
/**
 * @file
 * Template file for the message that is send if an E-Card is picked up.
 * 
 * This template is intentionally left blank.
 * override is for easy theming of html mails
 */

print $message;