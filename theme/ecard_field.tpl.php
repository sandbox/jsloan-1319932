<?php
/**
 * Tepmlate for E-card text display.
 */
?>

<div class="ecard">
  <div class="name-to">Hey <?php print $variables['ecard']->name_to ?>,</div>
  <div class="content"><?php print $variables['ecard']->text ?></div>
  <div class="content">Regards <?php print $variables['ecard']->name_from ?></div>
</div>