<?php

/**
 * @file E-Mail implementation for the E-Card module.
 */

/**
 *  Send the E-Card and it's copy.
 */
function ecard_mail_send($ecard, $instance,  $key = 'ecard_ecard') {
    global $base_url;    
    
    $ecard->mail = $instance['settings'];
    $ecard->site_name = variable_get('site_name', 'Default site name');

    _ecard_mail_send_ecard($ecard, $key);

    // copy to sender
    if ($ecard->copy_sender && $key != 'ecard_notice') {
        _ecard_mail_send_ecard($ecard, 'ecard_copy');
    }

    // copy to manager
    if ($ecard->mail_manager && $key != 'ecard_notice') {
        _ecard_mail_send_ecard($ecard, 'ecard_manager');
    }

}

/**
 * Helper function to select the appropriate card.
 */
function _ecard_mail_send_ecard($ecard, $key = 'ecard_ecard') {
  $module = 'ecard';
  // Specify 'to' and 'from' addresses.
  switch ($key) {
    case 'ecard_ecard':
      $ecard->ecard_url = url($ecard->entity_path, array('query' => array('ecard' => $ecard->hash), 'absolute' => TRUE));
      $to = _ecard_recipient($ecard->mail_to, $ecard->name_to);
      break;
    case 'ecard_copy':
      $ecard->ecard_url = url($ecard->entity_path, array('query' => array('copy' => $ecard->hash), 'absolute' => TRUE));
      $to = _ecard_recipient($ecard->mail_from, $ecard->name_from);
      break;
    case 'ecard_manager':
      $ecard->ecard_url = url($ecard->entity_path, array('query' => array('copy' => $ecard->hash), 'absolute' => TRUE));
      $to = _ecard_recipient($ecard->mail_manager, $ecard->name_manager);
      break;
    case 'ecard_notice':
      $ecard->ecard_url = url($ecard->entity_path, array('query' => array('copy' => $ecard->hash), 'absolute' => TRUE));
      $to = _ecard_recipient($ecard->mail_from, $ecard->name_from);
      break;
  }
  $language = $GLOBALS['language'];  
  $params['ecard'] = $ecard;
  if ($ecard->mail_from) {
    $from = _ecard_recipient($ecard->mail_from, $ecard->name_from);
  }
  $send = TRUE;
  // call hook_mail
  $message = drupal_mail($module, $key, $to, $language, $params, $from, $send);

  if ($message['result']){
      switch ($key) {
        case 'ecard_ecard':
            ecard_update_sent($ecard->hash);
            if (!$ecard->mail['ecard_settings']['ecard_cron']){
                 $message = t('Your ecard has been sent.');
                 drupal_set_message($message);
            }
        break;
        case 'ecard_copy':
            ecard_update_copy_sender($ecard->hash);
            if (!$ecard->mail['ecard_settings']['ecard_cron']){
                $message = t('A copy of this card has been sent to you.');
                drupal_set_message($message);
            }
        break;
        case 'ecard_manager':            
            if (!$ecard->mail['ecard_settings']['ecard_cron']){
                $message = t('A copy of this card has been sent the manager.');
                drupal_set_message($message);
            }
        break;
        case 'ecard_notice':
            ecard_update_notify_sender($ecard->hash);
        break;
      }
  }
  return $message['result']; 
}

/**
 * Helper function to create a RFC 2822 conform recipient.
 */
function _ecard_recipient($mail, $name = NULL) {
  if ($name) {
    return $name . ' <' . $mail . '>';
  }
  else {
    return $mail;
  }
}

/**
 * Default text for the E-Card letter.
 */
function _ecard_letter() {
  $output = t(
    "Hi [ecard:name_to],

[ecard:name_from] made an E-Card for you.
At any time you may see your card by clicking this link:

[ecard:ecard_url]

(if your email client doesn't allow you to click on the site link,
then just copy and paste the URL into your browser)

admin");
  return $output;
}

/**
 * Default text for the E-Card copy notice csmith@email.comletter.
 */
function _ecard_copy() {
  $output = t(
    "Hi [ecard:name_from],
Here is the copy of your message you sent to [ecard:name_to]

[ecard:ecard_url]

(if your email client doesn't allow you to click on the site link,
then just copy and paste the URL into your browser)

admin");
  return $output;
}

/**
 * Default text for the E-Card manager notice letter.
 */
function _ecard_manager() {
  $output = t(
    "Hi [ecard:name_manager],
Here is the copy of a message sent to [ecard:name_to] by [ecard:name_from]

[ecard:ecard_url]

(if your email client doesn't allow you to click on the site link,
then just copy and paste the URL into your browser)

admin");
  return $output;
}

/**
 * Default text for the E-Card pickup notification.
 */
function _ecard_notice() {
  $output = t(
    "Hi,

[ecard:name_to] has picked up your e-card today.
Here is the url to your e-card
[ecard:ecard_url]

(if your email client doesn't allow you to click on the site link,
then just copy and paste the URL into your browser)

admin");
  return $output;
}

