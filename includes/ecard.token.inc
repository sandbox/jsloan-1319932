<?php

/**
 * Implements hook_token_info().
 */
function ecard_token_info() {

  $types['ecard'] = array(
    'name' => t('E-Card'),
    'description' => t('Tokens for the E-Card module'),
  );

  $ecard['name_from'] = array(
    'name' => t('Name sender'),
    'description' => t('The name of the E-Card sender.'),
  );

  $ecard['mail_from'] = array(
    'name' => 'Mail sender',
    'description' => t('The mail adress of the E-Card sender.'),
  );

  $ecard['name_to'] = array(
    'name' => t('Name receiver'),
    'description' => t('The name of the E-Card receiver.'),
  );
  $ecard['mail_to'] = array(
    'name' => 'Mail receiver',
    'description' => t('The mail adress of the E-Card receiver.'),
  );

  $ecard['name_manager'] = array(
    'name' => t('Manager name'),
    'description' => t('The name of the manager for E-Card.'),
  );
  $ecard['mail_manager'] = array(
    'name' => 'Manager mail',
    'description' => t('The mail adress of the manager for E-Card.'),
  );

  $ecard['ecard_url'] = array(
    'name' => 'E-Card URL',
    'description' => t('The URL of the E-Card.'),
  );

  return array(
    'types' => $types,
    'tokens' => array(
      'ecard' => $ecard,
    ),
  );
}

/**
 * Implements hook_tokens().
 */
function ecard_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();
  if ($type === 'ecard') {
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'name_from':
          $replacements[$original] = $data['ecard']->name_from;
          break;
        case 'mail_from':
          $replacements[$original] = $data['ecard']->mail_from;
          break;
        case 'name_to':
          $replacements[$original] = $data['ecard']->name_to;
          break;
        case 'mail_to':
          $replacements[$original] = $data['ecard']->mail_to;
          break;
        case 'name_manager':
          $replacements[$original] = $data['ecard']->name_manager;
          break;
        case 'mail_manager':
          $replacements[$original] = $data['ecard']->mail_manager;
          break;
        case 'ecard_url':
          $replacements[$original] = $data['ecard']->ecard_url;
          break;
      }
    }
  }
  return $replacements;
}
