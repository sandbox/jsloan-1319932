<?php

/**
 * @file
 * Provides entity integration for E-Card module
 */

/**
 * Implements hook_entity_info.
 */
function ecard_entity_info() {
  return array(
    'ecard' => array(
      'label' => t('E-Card'),
      'entity class' => 'Entity',
      'controller class' => 'EntityAPIController',
      'base table' => 'ecard',
      'entity keys' => array(
        'id' => 'hash',
      ),
    ),
  );
}

/**
 * Creates and saves an E-Card entity.
 *
 * @param $ecard
 *  An array of E-Card values.
 *   Necessary keys:
 *    nid:        The node id refernce.
 *    mail_from:  the mail of the sender.
 *    name_from:  the name of the sender.
 *    mail_to:    the mail of the reciever.
 *    name_to:    the name of the reciever.
 *    text:       the content of the E-Card.
 *
 *   Optional keys:
 *    hash:       the unique identifier of the E-Card. Pass for update.
 *    pick_up:    flag if the ecard alredy picked up. Default 0.
 *
 * @return
 *  Returns the E-Card object if the entity was saved successfully.
 *  Otherwise it returns FALSE.
 */
function ecard_create($ecard) {  
  
  if (!ecard_check_hash($ecard)) {
    $ecard['timestamp'] = time();
    $hash_base = $ecard['mail_from'] . uniqid();
    $ecard['hash'] = md5($hash_base);
    $ecard = entity_create('ecard', $ecard);
  }

  if (is_array($ecard)) {
    $ecard = (object) $ecard;
  }

  if (entity_save('ecard', $ecard)) {
    return $ecard;
  }
  return FALSE;
}

/**
 * Deletes an E-Card entity by given hash value.
 *
 * @param $hash
 *  The hash value of the etity to be deleted.
 * 
 */
function ecard_delete($hash) {
  entity_delete('ecard', $hash);
}

/**
 * Deletes multiple E-Card entities by given hash values.
 *
 * @param array $hash
 *  An array of hash values of the etities to be deleted.
 * 
 */
function ecard_delete_multiple(array $hashes) {
  entity_delete_multiple('ecard', $hashes);
}

/**
 * Returns an E-Card entity by given hash value.
 *
 * @param $hash
 *  The hash value of the entity to be returned.
 *    Leave it NULL to recieve all E-Cards.
 *    Pass a string to recive one E-Card.
 *    Pass an array of hashes to recive X E-Cards.
 *
 * @return
 *  An arry with the called E-Card entitys.
 *  Returns FALSE if there are no entities with the given hash value.
 */
function ecard_read($hash = FALSE) {
  if (is_string($hash)) {
    $hash = array($hash);
  }

  return entity_load('ecard', $hash);
}

/**
 * Updates an E-Card entity.
 *
 * @param $ecard
 *  An array of E-Card values.
 *   Necessary keys:
 *    hash:       the unique identifier of the E-Card.
 *
 *   Optional keys:
 *    mail_from:  the mail of the sender.
 *    nid:        The node id refernce.
 *    name_from:  the name of the sender.
 *    mail_to:    the mail of the reciever.
 *    name_to:    the name of the reciever.
 *    text:       the content of the E-Card.
 *    pick_up:    flag if the ecard alredy picked up. Default 0.
 *
 * @return
 *  Returns the E-Card object if the entity was saved successfully.
 *  Otherwise it returns FALSE.
 */
function ecard_update($ecard) {
  return ecard_create($ecard);
  }

/**
 * Helper function to update E-Card when sent.
 *
 * @param $hash
 *   string of Ecard hash
 * 
 */

function ecard_update_sent($hash){
    $update = array('hash' => $hash, 'sent' => time());
    ecard_update($update);
}

/**
 * Helper function to update E-Card when copy sent.
 *
 * @param $hash
 *   string of Ecard hash
 * 
 */
function ecard_update_copy_sender($hash){
    $update = array('hash' => $hash, 'copy_sender' => time());
    ecard_update($update);
}

/**
 * Helper function to update E-Card when picked up.
 *
 * @param $hash
 *   string of Ecard hash
 * 
 */
function ecard_update_pick_up($hash){
    $update = array('hash' => $hash, 'pick_up' => time());
    // set only on the first pickup
    $ecard = ecard_read($hash);
    if ($ecard[$hash]->pick_up == 0){
        ecard_update($update);
    }
}

/**
 * Helper function to update E-Card when notification is sent.
 *
 * @param $hash
 *   string of Ecard hash
 * 
 */
function ecard_update_notify_sender($hash){
    $update = array('hash' => $hash, 'notify_sender' => time());
    ecard_update($update);
}

/**
 * Helper function to check the hash value of an E-Card.
 *
 * @param $ecard
 *   An array with the E-Card to check.
 *
 * @return
 *    FALSE if the hash value is not existing or with incorrect value.
 *    Otherwise it returns TRUE.
 */
function ecard_check_hash($ecard) {
  if (!array_key_exists('hash', $ecard)) {
    return FALSE;
  }
  if (!preg_match('~^[0-9a-f]{32}$~', $ecard['hash'])) {
    return FALSE;
  }
  return TRUE;
}

/**
 * Render the given E-Card
 *
 * @param $hash
 *  The E-Card to render.
 *
 * @return
 *  The html of the E-Card value.
 *
 * @todo
 *  Implementing theme functions.
 */
function ecard_render_ecard($hash, $args, $options) {
  $ecard_render = '';
  if ($options['show_form'] === 1 && $options['form_position'] === 'above') {
    $ecard_render .= drupal_render(drupal_get_form('ecard_form_create_ecard', $args));
  }
  if ($options['show_ecard'] === 1) {

    $ecards = ecard_read($hash);

    foreach ($ecards as $ecard) {

      if ($ecard->entity_type === $args['entity_type']
        && $ecard->bundle === $args['bundle']
        && $ecard->entity_id == $args['entity_id']) {

        $ecard->text = token_replace($ecard->text, (array) $ecard);
        $element['ecard'] = $ecard;
        $element['entity'] = entity_load($ecard->entity_type, array($ecard->entity_id));
        $ecard_render .= theme('ecard_formatter_view', $element);
      }
    }
  }
  if ($options['show_form'] === 1 && $options['form_position'] === 'below') {
    $ecard_render .= drupal_render(drupal_get_form('ecard_form_create_ecard', $args));
  }
  return $ecard_render;
}


/**
 * Helper function to update E-Card when email is sent.
 *
 * @param $hash
 *   string of Ecard hash
 * 
 */
function ecard_process_send($hash = null){
    global $base_url;

    $ecard_instance = field_read_instance('node', 'field_ecard', 'ecard');
    $ecard_max_count = $ecard_instance['settings']['ecard_settings']['ecard_max_count'];
    if ($ecard_max_count > 0) {
        $limit = sprintf('LIMIT 0, %s',$ecard_max_count);
    } else {
        $limit = '';
    }

    if ($hash) {
        $sql = sprintf("SELECT hash FROM {ecard} WHERE hash = '%s' and sent = 0", $hash);
    } else {
        $sql = sprintf('SELECT hash FROM {ecard} WHERE sent = 0 %s', $limit);
    }
    
    $result = db_query($sql);
    $list = $result->fetchAll();

    foreach ($list as $card){
        $ecard = ecard_read($card->hash);
        $nid = 'node/'.$ecard[$card->hash]->entity_id;
        $alias = drupal_lookup_path('alias',$nid);
        $path = ($alias)?$alias:$nid;
        $url = $base_url . '/' . $path;
        $ecard[$card->hash]->entity_path = $url;
        $status = ecard_mail_send($ecard[$card->hash],$ecard_instance);

    }

}

/**
 * Helper function to update E-Card when notification email is sent.
 *
 * @param $hash
 *   string of Ecard hash
 *
 */
function ecard_process_notify($hash = null){
    global $base_url;
    $ecard_instance = field_read_instance('node', 'field_ecard', 'ecard');
    
    if ($hash) {
        $sql = sprintf("SELECT hash FROM {ecard} WHERE hash = '%s' and notify_sender = 1",$hash);
    } else {
        $sql = 'SELECT hash FROM {ecard} WHERE pick_up > 0 and notify_sender = 1';
    }

    $result = db_query($sql);
    $list = $result->fetchAll();
    
    foreach ($list as $card){
        $ecard = ecard_read($card->hash);
        $nid = 'node/'.$ecard[$card->hash]->entity_id;
        $alias = drupal_lookup_path('alias',$nid);
        $path = ($alias)?$alias:$nid;
        $url = $base_url . '/' . $path;
        $ecard[$card->hash]->entity_path = $url;
        $status = ecard_mail_send($ecard[$card->hash],$ecard_instance,'ecard_notice');
    }

}
