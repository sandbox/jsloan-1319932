<?php

/**
 * @file
 * Field for the E-Card module to display the input form or an E-Card output.
 */

/**
 * Implements hook_field_info().
 */
function ecard_field_info() {
  return array(
    'ecard' => array(
      'label' => t('E-Card Form'),
      'description' => t('Create an E-Card Form for this content.'),
      'default_widget' => 'ecard_widget',
      'default_formatter' => 'ecard_formatter_html',
    ),
  );
}

/**
 * Implements hook_field_is_empty().
 */
function ecard_field_is_empty($item, $field) {
  return FALSE;
}

/**
 * Implements hook_field_widget_info().
 */
function ecard_field_widget_info() {
  return array(
    'ecard_widget' => array(
      'label' => t('Default'),
      'field types' => array('ecard'),
    ),
  );
}

/**
 * Implements hook_field_widget_form().
 */
function ecard_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {

  $element += array(
    '#type' => 'fieldset',
  );
  $element['id'] = array(
    '#title' => t('Id'),
    '#type' => 'textfield',
    '#description' => 'CSS id for the E-Card div.',
    '#default_value' => isset($items[0]['id']) ? $items[0]['id'] : '',
  );
  $element['class'] = array(
    '#title' => t('Class'),
    '#type' => 'textfield',
    '#description' => 'CSS class for the E-Card div.',
    '#default_value' => isset($items[0]['class']) ? $items[0]['class'] : 'ecard-message',
  );
  $element['top'] = array(
    '#title' => t('Top'),
    '#type' => 'textfield',
    '#description' => 'Top offset for the text in pixel.',
    '#default_value' => isset($items[0]['top']) ? $items[0]['top'] : 0,
  );
  $element['left'] = array(
    '#title' => t('Left'),
    '#type' => 'textfield',
    '#description' => 'Left offset for the text in pixel.',
    '#default_value' => isset($items[0]['left']) ? $items[0]['left'] : 0,
  );
  $element['width'] = array(
    '#title' => t('Width'),
    '#type' => 'textfield',
    '#description' => 'Width for the text in pixel.',
    '#default_value' => isset($items[0]['width']) ? $items[0]['width'] : 300,
  );
  $element['color'] = array(
    '#title' => t('Color'),
    '#type' => 'textfield',
    '#description' => 'Color of the E-Card message to be displayed in hexadecimal. Don\'t use "#"',
    '#default_value' => isset($items[0]['color']) ? $items[0]['color'] : '000000',
  );
  $element['font'] = array(
    '#title' => t('Font.'),
    '#type' => 'textfield',
    '#description' => 'Path to a TTF font file.',
    '#default_value' => isset($items[0]['font']) ? $items[0]['font'] : '',
  );
  $element['font_size'] = array(
    '#title' => t('Font size'),
    '#type' => 'textfield',
    '#description' => 'Font size for the text in pixel.',
    '#default_value' => isset($items[0]['font_size']) ? $items[0]['font_size'] : 12,
  );
  return $element;
}

/**
 * Implements hook_form_field_ui_field_edit_form_alter().
 * Remove unwanted multi instances field. api.drupal.org says this shiuld be
 * done in hook_field_widget_info() but this won't work. Got this from the
 * fivestar module.
 *
 * @param array $form
 * @param array $form_state
 */
function ecard_form_field_ui_field_edit_form_alter(&$form, $form_state) {
  $field = $form['#field'];
  if ($field['type'] == 'ecard') {
    // Multiple values is not supported with E-Card.
    $form['field']['cardinality']['#access'] = FALSE;
    $form['field']['cardinality']['#value'] = 1;
  }
}

/**
 * Settings for the field instance.
 *
 * @param type $field
 * @param type $instance
 * @return type
 */
function ecard_field_instance_settings_form($field, $instance) {    
  // Base config.
  $form['ecard_settings'] = array('#title' => t('Configuration'), '#type' => 'fieldset', '#collapsed' => FALSE, '#collapsible' => TRUE);

  $form['ecard_settings']['ecard_cron'] = array(
    '#type' => 'checkbox',
    '#title' => t('Send emails druring cron'),
    '#size' => '3',
    '#default_value' => isset($instance['settings']['ecard_settings']['ecard_cron']) ? $instance['settings']['ecard_settings']['ecard_cron'] : 0,
    '#description' => t('E-Cards will automatically be sent when submitted. To send E-Card with cron check this box.'),
    );
  
  $form['ecard_settings']['ecard_max_count'] = array(
    '#type' => 'textfield',
    '#title' => 'Maximum number of e-mails allowed to send at a time',
    '#size' => '3',
    '#default_value' => isset($instance['settings']['ecard_settings']['ecard_max_count']) ? $instance['settings']['ecard_settings']['ecard_max_count'] : 0,
    '#description' => 'The default 0 means that there will be no limit.',
    );
  
  // @todo: use tokens!
  $form['ecard_settings']['ecard_redirect'] = array(
    '#type' => 'textfield',
    '#title' => t('Redirection after the E-Card was send.'),
    '#default_value' => isset($instance['settings']['ecard_settings']['ecard_redirect']) ? $instance['settings']['ecard_settings']['ecard_redirect'] : '',
    '#description' => t('Enter the path to where users should be redirected to after sending an E-Card.  For example <em>node/123</em>. Leave blank for redirection to the submitted E-Card.'),
  );
  $form['ecard_settings']['ecard_require_name'] = array(
    '#type' => 'checkbox',
    '#title' => t('Require the sender to provide their name.'),
    '#default_value' => isset($instance['settings']['ecard_settings']['ecard_require_name']) ? $instance['settings']['ecard_settings']['ecard_require_name'] : 0,
  );
  $form['ecard_settings']['ecard_require_message'] = array(
    '#type' => 'checkbox',
    '#title' => t('Require the sender to enter a personal message.'),
    '#default_value' => isset($instance['settings']['ecard_settings']['ecard_require_message']) ? $instance['settings']['ecard_settings']['ecard_require_message'] : 0,
  );
  $form['ecard_settings']['ecard_fill_in_name_e-mail'] = array(
    '#type' => 'checkbox',
    '#title' => t('Automatically fill in sender name and e-mail for logged in users.'),
    '#default_value' => isset($instance['settings']['ecard_settings']['ecard_fill_in_name_e-mail']) ? $instance['settings']['ecard_settings']['ecard_fill_in_name_e-mail'] : 0,
  );

  // E-Card Delivery message.
  $form['letter'] = array('#title' => t('Delivery message'), '#type' => 'fieldset', '#collapsed' => TRUE, '#collapsible' => TRUE);
  $form['letter']['ecard_subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Subject'),
    '#size' => 70,
    '#maxlength' => 70,
    '#default_value' => isset($instance['settings']['letter']['ecard_subject']) ? $instance['settings']['letter']['ecard_subject'] : 'E-Card from [site:name]',
    '#description' => t('Customize the subject for E-Card'),
  );
  $form['letter']['ecard_letter'] = array(
    '#type' => 'textarea',
    '#title' => t('Body'),
    '#cols' => 70,
    '#rows' => 5,
    '#default_value' => isset($instance['settings']['letter']['ecard_letter']) ? $instance['settings']['letter']['ecard_letter'] : _ecard_letter(),
    '#description' => t('This text is the body of the e-mail that the E-Card recipient will see. These are the variables you may use: %site = your site name, %site_url = your site URL, %site_mail = your site e-mail address, %card_url = the URL for the E-Card, %sender = sender name, %sender_e-mail = sender e-mail, %recipient = recipient e-mail'),
  );

  // E-Card copy notification.
  $form['copy'] = array('#title' => t("Sender's copy message"), '#type' => 'fieldset', '#collapsed' => TRUE, '#collapsible' => TRUE);
  $form['copy']['ecard_copy_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Send a copy of E-Cards to sender'),
    '#default_value' => isset($instance['settings']['copy']['ecard_copy_enabled']) ? $instance['settings']['copy']['ecard_copy_enabled'] : TRUE,
  );
  $form['copy']['ecard_copy_subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Subject'),
    '#size' => 70,
    '#maxlength' => 70,
    '#default_value' => isset($instance['settings']['copy']['ecard_copy_subject']) ? $instance['settings']['copy']['ecard_copy_subject'] : 'A copy of your E-Card',
    '#description' => t('Customise the e-mail sent to the sender as copy of the E-Card.'),
  );
  $form['copy']['ecard_copy'] = array(
    '#type' => 'textarea',
    '#title' => t('Body'),
    '#cols' => 70,
    '#rows' => 5,
    '#default_value' => isset($instance['settings']['copy']['ecard_copy']) ? $instance['settings']['copy']['ecard_copy'] : _ecard_copy(),
    '#description' => t('This text is the body of the e-mail to notice the sender that his E-Card has been picked up. These are the variables you may use: %site = your site name, %site_url = your site URL, %site_mail = your site e-mail address, %card_url = the URL for the E-Card'),
  );

  // Notification settings.
    $form['notice'] = array('#title' => t('Pickup notification message'), '#type' => 'fieldset', '#collapsed' => TRUE, '#collapsible' => TRUE);
    $form['notice']['ecard_notice_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable option for E-Card pickup notification e-mails'),
    '#default_value' => isset($instance['settings']['notice']['ecard_notice_enabled']) ? $instance['settings']['notice']['ecard_notice_enabled'] : TRUE,
    );
    $form['notice']['ecard_notice_subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Subject'),
    '#size' => 70,
    '#maxlength' => 70,
    '#default_value' => isset($instance['settings']['notice']['ecard_notice_subject']) ? $instance['settings']['notice']['ecard_notice_subject'] : 'Your E-Card has been just picked up',
    '#description' => t('Customize the subject for E-Card'),
    );
    $form['notice']['ecard_notice'] = array(
    '#type' => 'textarea',
    '#title' => t('Body'),
    '#cols' => 70,
    '#rows' => 5,
    '#default_value' => isset($instance['settings']['notice']['ecard_notice']) ? $instance['settings']['notice']['ecard_notice'] : _ecard_notice(),
    '#description' => t('This text is the body of the e-mail to notice the sender that the E-Card has been picked up. These are the variables you may use: %site = your site name, %site_url = your site URL, %site_mail = your site e-mail address, %card_url = the URL for the E-Card, %sender = sender name, %recipient = recipient e-mail'),
    );


 // E-Card copy manager.
  $form['manager'] = array('#title' => t("Manager's copy message"), '#type' => 'fieldset', '#collapsed' => TRUE, '#collapsible' => TRUE);

  $form['manager']['ecard_manager_subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Subject'),
    '#size' => 70,
    '#maxlength' => 70,
    '#default_value' => isset($instance['settings']['manager']['ecard_manager_subject']) ? $instance['settings']['manager']['ecard_manager_subject'] : 'A copy of an E-Card',
    '#description' => t('Customise the e-mail sent to the manager as copy of the E-Card.'),
  );
  $form['manager']['ecard_manager'] = array(
    '#type' => 'textarea',
    '#title' => t('Body'),
    '#cols' => 70,
    '#rows' => 5,
    '#default_value' => isset($instance['settings']['manager']['ecard_manager']) ? $instance['settings']['manager']['ecard_manager'] : _ecard_manager(),
    '#description' => t('This text is the body of the e-mail to notify the manager that an E-Card has been sent. These are the variables you may use: %site = your site name, %site_url = your site URL, %site_mail = your site e-mail address, %card_url = the URL for the E-Card'),
  );

  
  return $form;
}


 
/**
 * Implements hook_field_formatter_info().
 */
function ecard_field_formatter_info() {
  return array(
    'ecard_formatter_html' => array(
      'label' => t('View'),
      'field types' => array('ecard'),
      'settings' => array('show_ecard' => 1, 'show_form' => 0, 'form_position' => 'below'),
    ),
    'ecard_formatter_image' => array(
      'label' => t('E-Card image'),
      'field types' => array('image'),
      'settings' => array('image_style' => '',),
    ),
  );
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function ecard_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

// Settings for E-Card field formater.
  if ($field['type'] === 'ecard') {
    $element['show_ecard'] = array(
      '#title' => t('Show ecard text'),
      '#type' => 'checkbox',
      '#description' => t('Show the E-Card text if the E-Card is available. You can also show the E-Card by an image field.'),
      '#default_value' => $settings['show_ecard'],
    );
    $element['show_form'] = array(
      '#title' => t('Show form'),
      '#type' => 'checkbox',
      '#description' => t('Show the E-Card form if an E-Card is available. If no E-Card is available the form is always been shown.'),
      '#default_value' => $settings['show_form'],
    );
    $element['form_position'] = array(
      '#title' => t('Form position'),
      '#type' => 'select',
      '#description' => 'If the form is shown, where shall it apear.',
      '#options' => array('above' => t('Above the E-Card text'), 'below' => t('Below the E-Card text')),
      '#default_value' => $settings['form_position'],
    );
  }

  // Settings for image field formatter. Copy from image.field.inc
  if ($field['type'] === 'image') {
    $image_styles = image_style_options(FALSE);
    $element['image_style'] = array(
      '#title' => t('Image style'),
      '#type' => 'select',
      '#default_value' => $settings['image_style'],
      '#empty_option' => t('None (original image)'),
      '#options' => $image_styles,
    );
  }

  return $element;
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function ecard_field_formatter_settings_summary($field, $instance, $view_mode) {

  $settings = $instance['display'][$view_mode]['settings'];
  $summary = array();

  // Summary for E-Card field.
  if ($field['type'] === 'ecard') {
    $summary[] = t('Show ecard: ') . $settings['show_ecard'];
    $summary[] = t('Show form: ') . $settings['show_form'];
    $summary[] = t('Form position: ') . $settings['form_position'];
  }

  // Summary for image field. Copy from image.field.inc
  if ($field['type'] === 'image') {
    $image_styles = image_style_options(FALSE);
    // Unset possible 'No defined styles' option.
    unset($image_styles['']);
    // Styles could be lost because of enabled/disabled modules that defines
    // their styles in code.
    if (isset($image_styles[$settings['image_style']])) {
      $summary[] = t('Image style: @style', array('@style' => $image_styles[$settings['image_style']]));
    }
    else {
      $summary[] = t('Original image');
    }
  }
  return implode('<br />', $summary);
}

/**
 * Implements hook_field_formatter_view().
 *
 * @todo
 *  Better switching of formatters.
 */
function ecard_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {

  $options = $display['settings'];

  $args = ecard_get_args($entity_type, $entity, $field, $instance);

  $element = array();

  $hash = ecard_get_hash();


  $display_settings = ECARD_NO_HASH_SET;
  // add jQuery Autocomplete
  drupal_add_library('system', 'ui.autocomplete');
  // Add local stylesheet for auto-complete
  drupal_add_js(drupal_get_path('module', 'ecard') . '/js/auto-complete.js');
  drupal_add_css(drupal_get_path('module', 'ecard') . '/css/auto-complete.css');

  if ($hash) {
    $display_settings = ECARD_HASH_SET; 
  }


  if ($field['type'] === 'ecard') {
    $display_settings = $display_settings | ECARD_ECARD_FIELD;
  }
  if ($field['type'] === 'image') {
    $display_settings = $display_settings | ECARD_IMAGE_FIELD;
  }



  switch ($display_settings) {

    case ECARD_ECARD_FIELD | ECARD_NO_HASH_SET:
      switch ($display['type']) {
        case 'ecard_formatter_html':
          $args['mail'] = array(
            'letter' => $instance['settings']['letter'],
            'copy' => $instance['settings']['copy'],
          );
          $element[0] = drupal_get_form('ecard_form_create_ecard', $args);
          break;
      }
      break;

    case ECARD_IMAGE_FIELD | ECARD_NO_HASH_SET:
      foreach ($items as $delta => $item) {
        $element[$delta] = array(
          '#theme' => 'image_formatter',
          '#item' => $item,
          '#image_style' => $display['settings']['image_style'],
        );
      }
      break;

    case ECARD_ECARD_FIELD | ECARD_HASH_SET:
      $element[0]['#markup'] = ecard_render_ecard($hash, $args, $options);
      break;

    case ECARD_IMAGE_FIELD | ECARD_HASH_SET:       
      foreach ($items as $delta => $item) {

        $element[$delta] = array(
          '#theme' => 'ecard_image_formatter',
          '#item' => $item,
          '#image_style' => $display['settings']['image_style'],
          '#hash' => $hash,
          '#args' => $args,
          '#ecard_settings' => $entity->field_ecard['und'][0],
        );
      }
      break;
  }
  return $element;
}

/**
 * @return
 *   The valid hash of an E-Card. If no valid hash is available it returns FALSE.
 */
function ecard_get_hash() {

  // request is for either ecard or copy
  $ecard = isset($_GET['ecard'])?$_GET['ecard']:false;
  $copy = isset($_GET['copy'])?$_GET['copy']:false;

  // get the value passed by either parameter
  $value = $ecard?$ecard:$copy;

  // test to make sure the value is a valid hash
  $hash = ($value && preg_match('~^[0-9a-f]{32}$~', $value))?$value:false;

  // if an ecard then update "pick_up" and send notification if not using cron
  if ($hash && $ecard){
    // update pick_up
    ecard_update_pick_up($hash);

    // get the ecard entity settings
    $ecard_instance = field_read_instance('node', 'field_ecard', 'ecard');
    // get the cron setting
    $ecard_cron = $ecard_instance['settings']['ecard_settings']['ecard_cron'];

    if (!$ecard_cron) {
        ecard_process_notify($hash);
    }

    //

  }
  
  return $hash;
}

/**
 * Returns an array of arguments for an E-Card. This arguments are necessary 
 * to save or view an E-Card with a formatter.
 * @param $entity
 *   The entity, where the ecard is saved in.
 * @param $field
 *   The field array, for the E-Card or the image field.
 * @param $instance
 *   The instance array of the $field field.
 * 
 * @return
 *   An array of arguments for E-Cards.
 */
function ecard_get_args($entity_type, $entity, $field, $instance) {

  $entity_ids = entity_extract_ids($instance['entity_type'], $entity);
  $entity_uri = entity_uri($entity_type, $entity);

  $args = array(
    'entity_type' => $instance['entity_type'],
    'bundle' => $instance['bundle'],
    'entity_id' => $entity_ids[0],
    'entity_path' => $entity_uri['path'],
  );

  if (!empty($instance['settings']['ecard_settings']['ecard_redirect'])) {
    $args['custom_path'] = $instance['settings']['ecard_settings']['ecard_redirect'];
  }
  return $args;
}

/**
 * The E-Card creation form. Called by drupal_get_form
 *
 * @return
 *  The E-Card creation form.
 */
function ecard_form_create_ecard($form, &$form_state) {
global $user;

// get the ecard settings
$ecard_instance = field_read_instance('node', 'field_ecard', 'ecard');
$require_name = $ecard_instance['settings']['ecard_settings']['ecard_require_name'];
$require_message = $ecard_instance['settings']['ecard_settings']['ecard_require_message'];
$fill_in = $ecard_instance['settings']['ecard_settings']['ecard_fill_in_name_e-mail'];

//die('<pre>' . print_r($ecard_instance,1));
  // Sender
  $form['sender'] = array(
      '#type' => 'fieldset',
      '#title' => t('Sender'),
      '#weight' => 1,
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
  );

  // Sender's name.
  $form['sender']['name_from'] = array(
    '#title' => t('Sender name'),
    '#type' => 'textfield',
    '#required' => ($require_name == 1)?TRUE:FALSE,
    '#id' => 'Sender_name',
    '#default_value' => ($fill_in == 1 && $user->uid != 0)?$user->name:'',
    //'#tree' => FALSE,
  );

  // Sender's e-mail.
  $form['sender']['mail_from'] = array(
    '#title' => t('Sender e-mail'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => ($fill_in == 1 && $user->uid != 0)?$user->mail:'',
    //'#tree' => FALSE,
  );

  $form['sender']['ecard_copy'] = array(
    '#type' => 'checkbox',
    '#title' => t('Send me a copy of this card'),
    );
  $form['sender']['ecard_notice'] = array(
    '#type' => 'checkbox',
    '#title' => t('Notify me when this card is picked up'),
    );


  // Recipients
  $form['recipients'] = array(
      '#type' => 'fieldset',
      '#title' => t('Recipients'),
      '#weight' => 2,
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
  );

  // Recipients name.
  $form['recipients']['name_to'] = array(
    '#title' => t('Recipients name'),
    '#type' => 'textfield',
    '#id' => 'Recipients_name',
    //'#tree' => FALSE,
  );

  // Recipients e-mail.
  $form['recipients']['mail_to'] = array(
    '#title' => t('Recipients e-mail'),
    '#type' => 'textfield',
    '#required' => TRUE,
    //'#tree' => FALSE,
  );

  // Manager
  $form['manager'] = array(
      '#type' => 'fieldset',
      '#title' => t('Manager'),
      '#weight' => 3,
      '#collapsible' => TRUE,
      '#collapsed' => True,
  );

  // Manager name.
  $form['manager']['name_manager'] = array(
    '#title' => t('Manager name'),
    '#type' => 'textfield',
    '#id' => 'Manager_name',
    //'#tree' => FALSE,
  );

  // Manager e-mail.
  $form['manager']['mail_manager'] = array(
    '#title' => t('Manager e-mail'),
    '#type' => 'textfield',
    '#required' => false,
    //'#tree' => FALSE,
  );


  /**
   * @todo Implement a switch between single and multiple recipients.
   * 
    // Display box to type recipients e-mails.
    $form['mail_to'] = array(
    '#title' => t('E-mail(s) of recipient(s)'),
    '#type' => 'textarea',
    '#rows' => 3,
    '#default_value' => '',
    '#description' => t('You may enter multiple e-mails separated by commas or line breaks'),
    '#required' => TRUE,
    '#tree' => FALSE,
    );
   */
  // Display textarea to type message.
  $form['text'] = array(
    '#title' => t('Your message'),
    '#type' => 'textarea',
    '#weight' => 4,
    '#description' => t('Whatever you type here will be displayed with the E-Card'),
    '#required' => ($require_message == 1)?TRUE:FALSE,
    //'#tree' => FALSE,
  );

  // Filter format for the text.
  // @see http://drupal.org/update/modules/6/7#text_format
  //$form['filter'] = filter_form();

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Send This E-Card'),
    '#weight' => 100
  );

  // add jQuery Autocomplete
  drupal_add_library('system', 'ui.autocomplete');
  // Add local stylesheet for auto-complete
  drupal_add_js(drupal_get_path('module', 'ecard') . '/js/auto-complete.js');
  drupal_add_css(drupal_get_path('module', 'ecard') . '/css/auto-complete.css');

  
  return $form;
}

/**
 * Validation of the form ecard_form_create_ecard.
 */
function ecard_form_create_ecard_validate($form, &$form_state) {
  if (!valid_email_address($form['sender']['mail_from']['#value'])) {
    form_set_error('', t('You must enter a valid e-mail adress.'));
  }
  if (!valid_email_address($form['recipients']['mail_to']['#value'])) {
    form_set_error('', t('You must enter a valid e-mail adress.'));
  }
}

/**
 * Submit of the form ecard_form_create_ecard.
 * @todo tree = false?
 */
function ecard_form_create_ecard_submit($form, &$form_state) {
global $base_url;

  $ecard['entity_type'] = $form_state['build_info']['args'][0]['entity_type'];
  $ecard['bundle'] = $form_state['build_info']['args'][0]['bundle'];
  $ecard['entity_id'] = $form_state['build_info']['args'][0]['entity_id'];
  $ecard['uid'] = $GLOBALS['user']->uid;

  // Remove any HTML and other unwanted chars that might destroy the senders name.
  $ecard['name_from'] = str_replace(array(',', '<', '>'), '', check_plain($form['sender']['name_from']['#value']));
  $ecard['mail_from'] = $form['sender']['mail_from']['#value'];
  $ecard['copy_sender'] = $form['sender']['ecard_copy']['#value'];
  $ecard['notify_sender'] = $form['sender']['ecard_notice']['#value'];
  
  $ecard['name_to'] = str_replace(array(',', '<', '>'), '', check_plain($form['recipients']['name_to']['#value']));
  $ecard['mail_to'] = $form['recipients']['mail_to']['#value'];

  $ecard['name_manager'] = str_replace(array(',', '<', '>'), '', check_plain($form['manager']['name_manager']['#value']));
  $ecard['mail_manager'] = $form['manager']['mail_manager']['#value'];

  // Make sure nothing bad can happen.
  // @todo Implement input filter system.
  $ecard['text'] = filter_xss_admin($form['text']['#value']);

  // ecard db insert
  $ecard = ecard_create($ecard);

  // get the ecard entity settings
  $ecard_instance = field_read_instance('node', 'field_ecard', 'ecard');
  $ecard_cron = $ecard_instance['settings']['ecard_settings']['ecard_cron'];

  $nid = 'node/'.$ecard->entity_id;
  $alias = drupal_lookup_path('alias',$nid);
  $path = ($alias)?$alias:$nid;
  $url = $base_url . '/' . $path;
  $ecard->entity_path = $url;

  if (!$ecard_cron){
    // send the card now!
    //ecard_mail_send($ecard,$ecard_instance);
    ecard_process_send($ecard->hash);
  }

  if (!empty($form_state['build_info']['args'][0]['custom_path'])) {
    drupal_goto($form_state['build_info']['args'][0]['custom_path']);
  }
  else {
    if ($ecard_cron){
        $message = t('Your E-Card has been submitted and will be sent soon.');
        drupal_set_message($message);
        if ($ecard->copy_sender == 1){
            $message = t('A copy of this card will be sent to you.');
            drupal_set_message($message);
        }
        if ($ecard->mail_manager){
            $message = t('A copy of this card will be sent to the manager.');
            drupal_set_message($message);
        }

    }
    if ($ecard->notify_sender == 1){
            $message = t('You will recieve an email notification when the card is picked up.');
            drupal_set_message($message);
    }

    drupal_goto($ecard->entity_path, array('query' => array('copy' => $ecard->hash)));
  }

}

