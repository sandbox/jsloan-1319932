<?php

$filname = "demodata.csv";
$data = array();
$row = 0;
if (($handle = fopen($filname, "r")) !== FALSE) {
    while (($lines = fgetcsv($handle, 1000, ",")) !== FALSE) {
        $num = count($lines);
        $record = array();
        for ($c=0; $c < $num; $c++) {
            $record[] = trim($lines[$c]);
        }

        $row++;
        if ($row == 1){
            $header = $record;
        } else {
            for ($c=0; $c < $num; $c++){
                $subdata[trim($header[$c])] = $record[$c];
            }
            $data[] = $subdata;
        }
    }
    fclose($handle);
}

header('Content-Type: text/javascript; charset=utf8');
echo json_encode($data);
?>
