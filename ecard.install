<?php

/**
 * @file
 * Install file for the ecard module
 */

/**
 * Implements hook_uninstall().
 */
function ecard_schema() {
  $schema['ecard'] = array(
    'description' => 'The table for E-Cards.',
    'fields' => array(
      'hash' => array(
        'description' => 'The hash-value unique identifier.',
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'default' => '',
      ),
      'entity_type' => array(
        'description' => 'The entity type of the parent entity the E-Card is saved in.',
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'default' => '',
      ),
      'bundle' => array(
        'description' => 'The bundle of the parent entity the entity is saved in.',
        'type' => 'varchar',
        'length' => 128,
        'not null' => TRUE,
        'default' => '',
      ),
      'entity_id' => array(
        'description' => 'The entity:bundle:id of the parent entity the E-Card is saved in.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'uid' => array(
        'description' => 'The user id of the author of the E-Card.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'mail_from' => array(
        'description' => 'The mail of the sender.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'name_from' => array(
        'description' => 'The name of the sender.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'mail_to' => array(
        'description' => 'The mail of the reciever.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'name_to' => array(
        'description' => 'The name of the reciever.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'mail_manager' => array(
        'description' => 'The mail of the manager.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'name_manager' => array(
        'description' => 'The name of the manager.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'text' => array(
        'description' => 'The content of the ecard.',
        'type' => 'text',
        'size' => 'medium',
        'not null' => TRUE,
      ),
      'copy_sender' => array(
        'description' => 'Flag: Time copy sent, 1=send a copy, 0=do not send copy',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'notify_sender' => array(
        'description' => 'Flag: Time notice sent, 1=send notice, 0=do not send notice.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'sent' => array(
        'description' => 'Flag: Time sent, 0=not sent',
        'type' => 'int',        
        'not null' => TRUE,
        'default' => 0,
      ),
      'pick_up' => array(
        'description' => 'Flag: Time picked up, 0=not picked up',
        'type' => 'int',        
        'not null' => TRUE,
        'default' => 0,
      ),
      'timestamp' => array(
        'description' => 'The timestamp of the ecard.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'indexes' => array(
      'entity_type' => array('entity_type'),
      'bundle' => array('bundle'),
      'entity_id' => array('entity_id'),
      'uid' => array('uid'),
    ),
    'primary key' => array('hash'),
  );
  return $schema;
}

/**
 * Implements hook_field_schema().
 */
function ecard_field_schema($field) {
  $columns = array(
    'id' => array(
      'description' => 'CSS ID for the E-Card div.',
      'type' => 'varchar',
      'not null' => TRUE,
      'length' => 255,
      'default' => '',
    ),
    'class' => array(
      'description' => 'CSS class for the E-Card div.',
      'type' => 'varchar',
      'not null' => TRUE,
      'length' => 255,
      'default' => '',
    ),
    'top' => array(
      'description' => 'Top offset of the E-Card text in pixel.',
      'type' => 'int',
      'size' => 'small',
      'not null' => TRUE,
      'default' => 0,
      'unsigned' => TRUE,
    ),
    'left' => array(
      'description' => 'Left offset of the E-Card text in pixel.',
      'type' => 'int',
      'size' => 'small',
      'not null' => TRUE,
      'default' => 0,
      'unsigned' => TRUE,
    ),
    'width' => array(
      'description' => 'Width of the E-Card text in pixel.',
      'type' => 'int',
      'size' => 'small',
      'not null' => TRUE,
      'default' => 300,
      'unsigned' => TRUE,
    ),
    'color' => array(
      'description' => 'Color of the E-Card message to be displayed in hexadecimal.',
      'type' => 'varchar',
      'not null' => TRUE,
      'length' => 6,
      'default' => '000000',
    ),
    'font' => array(
      'description' => 'Path to a font file to be used to render the message.',
      'type' => 'varchar',
      'not null' => TRUE,
      'length' => 255,
      'default' => '',
    ),
    'font_size' => array(
      'description' => 'Font size of the E-Card text in pixel.',
      'type' => 'int',
      'size' => 'small',
      'not null' => TRUE,
      'default' => 12,
      'unsigned' => TRUE,
    ),
  );
  return array(
    'columns' => $columns,
  );
}

/**
* Sandbox Update
*/
function ecard_update_7001(){

    db_add_field('ecard',
                 'mail_manager',
                 array('description' => 'The mail of the manager.',
                       'type' => 'varchar',
                       'length' => 255,
                       'not null' => TRUE,
                       'default' => '',
                      )
                );

    db_add_field('ecard',
                 'name_manager',
                 array('description' => 'The name of the manager.',
                       'type' => 'varchar',
                       'length' => 255,
                       'not null' => TRUE,
                       'default' => '',
                      )
                );


    db_add_field('ecard',
                 'copy_sender',
                 array('description' => 'Flag: Time copy sent, 1=send a copy, 0=do not send copy',
                                        'type' => 'int',
                                        'not null' => TRUE,
                                        'default' => 0,
                      )
                );

    db_add_field('ecard',
                 'notify_sender',
                 array('description' => 'Flag: Time notification sent, 1=send a notification, 0=do not send notification',
                                        'type' => 'int',
                                        'not null' => TRUE,
                                        'default' => 0,
                      )
                );

    db_add_field('ecard',
                 'sent',
                 array('description' => 'Flag: Time sent,  0=not yet sent',
                                        'type' => 'int',
                                        'not null' => TRUE,
                                        'default' => 0,
                      )
                );

    db_change_field('ecard',
                    'pick_up',
                    'pick_up',
                    array('type' => 'int',
                          'not null' => TRUE)
                   );

}