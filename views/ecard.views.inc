<?php

/**
 * @file
 * Conect our ecard entity with views by hook_views_data().
 */

/**
 * Implements hook_views_data().
 *
 * @todo Add joins to other entity tables. There are no joins to other tables
 * because we would have to make sure the entity type is respected.
 */
function ecard_views_data() {
  $data['ecard']['table']['group'] = t('E-Card');

  $data['ecard']['table']['base'] = array(
    'field' => 'hash',
    'title' => t('E-Card'),
    'help' => t('E-Cards created on this site.'),
  );

  $string_field = array(
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );

  $numeric_field = array('sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
    'filter' => array(
      'handler' => 'views_handler_field_numeric',
    ),
  );

  $data['ecard']['hash'] = array(
    'title' => t('Hash'),
    'help' => t('The hash identifier of an E-Card.'),
  );
  $data['ecard']['hash'] += $string_field;

  $data['ecard']['entity_type'] = array(
    'title' => t('Entity Type'),
    'help' => t('The entity type the E-Card is saved in.'),
  );
  $data['ecard']['entity_type'] += $string_field;

  $data['ecard']['bundle'] = array(
    'title' => t('Entity Bundle'),
    'help' => t('The bundle the entity is saved in.'),
  );
  $data['ecard']['bundle'] += $string_field;

  $data['ecard']['entity_id'] = array(
    'title' => t('Entity ID'),
    'help' => t('The entity:bundle:id the E-Card is saved in.'),
  );
  $data['ecard']['entity_id'] += $numeric_field;

  $data['ecard']['mail_from'] = array(
    'title' => t('Mail From'),
    'help' => t('The mail of the sender.'),
  );
  $data['ecard']['mail_from'] += $string_field;

  $data['ecard']['name_from'] = array(
    'title' => t('Name From'),
    'help' => t('The name of the sender.'),
  );
  $data['ecard']['name_from'] += $string_field;

  $data['ecard']['mail_to'] = array(
    'title' => t('Mait to'),
    'help' => t('The mail of the reciever.'),
  );
  $data['ecard']['mail_to'] += $string_field;

  $data['ecard']['name_to'] = array(
    'title' => t('Name to'),
    'help' => t('The name of the reciever.'),
  );
  $data['ecard']['name_to'] += $string_field;

  $data['ecard']['text'] = array(
    'title' => t('Text'),
    'help' => t('The content of the ecard.'),
  );
  $data['ecard']['text'] += $string_field;

  $data['ecard']['sent'] = array(
    'title' => t('Sent'),
    'help' => t('eCard Sent.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  $data['ecard']['pick_up'] = array(
    'title' => t('PickUp'),
    'help' => t('eCard Picked Up.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );


  $data['ecard']['timestamp'] = array(
    'title' => t('Timestamp'),
    'help' => t('The timestamp of the ecard.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  return $data;
}
